package com.line.bot.sync;

import com.line.bot.model.Good;
import com.line.bot.model.Location;
import com.line.bot.repository.GoodRepository;
import com.line.bot.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class ChangwatDataSynchronizer {

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    GoodRepository goodRepository;

    @Value("${changwat-data.surath-thani.location.name1}")
    private String surathLocationName1;

    @Value("${changwat-data.surath-thani.location.name2}")
    private String surathLocationName2;

    @Value("${changwat-data.surath-thani.location.name3}")
    private String surathLocationName3;

    @Value("${changwat-data.surath-thani.location.name4}")
    private String surathLocationName4;

    @Value("${changwat-data.surath-thani.location.name5}")
    private String surathLocationName5;

    @Value("${changwat-data.surath-thani.location.desc1}")
    private String surathLocationDesc1;

    @Value("${changwat-data.surath-thani.location.desc2}")
    private String surathLocationDesc2;

    @Value("${changwat-data.surath-thani.location.desc3}")
    private String surathLocationDesc3;

    @Value("${changwat-data.surath-thani.location.desc4}")
    private String surathLocationDesc4;

    @Value("${changwat-data.surath-thani.location.desc5}")
    private String surathLocationDesc5;

    @Value("${changwat-data.surath-thani.good.name1}")
    private String surathGoodName1;

    @Value("${changwat-data.surath-thani.good.name2}")
    private String surathGoodName2;

    @Value("${changwat-data.surath-thani.good.name3}")
    private String surathGoodName3;

    @Value("${changwat-data.surath-thani.good.name4}")
    private String surathGoodName4;

    @Value("${changwat-data.surath-thani.good.name5}")
    private String surathGoodName5;

    @Value("${changwat-data.surath-thani.good.desc1}")
    private String surathGoodDesc1;

    @Value("${changwat-data.surath-thani.good.desc2}")
    private String surathGoodDesc2;

    @Value("${changwat-data.surath-thani.good.desc3}")
    private String surathGoodDesc3;

    @Value("${changwat-data.surath-thani.good.desc4}")
    private String surathGoodDesc4;

    @Value("${changwat-data.surath-thani.good.desc5}")
    private String surathGoodDesc5;

    @Value("${changwat-data.chiangmai.location.name1}")
    private String chiangmaiLocationName1;

    @Value("${changwat-data.chiangmai.location.name2}")
    private String chiangmaiLocationName2;

    @Value("${changwat-data.chiangmai.location.name3}")
    private String chiangmaiLocationName3;

    @Value("${changwat-data.chiangmai.location.name4}")
    private String chiangmaiLocationName4;

    @Value("${changwat-data.chiangmai.location.name5}")
    private String chiangmaiLocationName5;

    @Value("${changwat-data.chiangmai.location.desc1}")
    private String chiangmaiLocationDesc1;

    @Value("${changwat-data.chiangmai.location.desc2}")
    private String chiangmaiLocationDesc2;

    @Value("${changwat-data.chiangmai.location.desc3}")
    private String chiangmaiLocationDesc3;

    @Value("${changwat-data.chiangmai.location.desc4}")
    private String chiangmaiLocationDesc4;

    @Value("${changwat-data.chiangmai.location.desc5}")
    private String chiangmaiLocationDesc5;

    @Value("${changwat-data.chiangmai.good.name1}")
    private String chiangmaiGoodName1;

    @Value("${changwat-data.chiangmai.good.name2}")
    private String chiangmaiGoodName2;

    @Value("${changwat-data.chiangmai.good.name3}")
    private String chiangmaiGoodName3;

    @Value("${changwat-data.chiangmai.good.name4}")
    private String chiangmaiGoodName4;

    @Value("${changwat-data.chiangmai.good.name5}")
    private String chiangmaiGoodName5;

    @Value("${changwat-data.chiangmai.good.desc1}")
    private String chiangmaiGoodDesc1;

    @Value("${changwat-data.chiangmai.good.desc2}")
    private String chiangmaiGoodDesc2;

    @Value("${changwat-data.chiangmai.good.desc3}")
    private String chiangmaiGoodDesc3;

    @Value("${changwat-data.chiangmai.good.desc4}")
    private String chiangmaiGoodDesc4;

    @Value("${changwat-data.chiangmai.good.desc5}")
    private String chiangmaiGoodDesc5;

    @Transactional
    public void forceSync() {
        locationRepository.save(new Location(surathLocationName1,surathLocationDesc1,"สุราษฎร์ธานี"));
        locationRepository.save(new Location(surathLocationName2,surathLocationDesc2,"สุราษฎร์ธานี"));
        locationRepository.save(new Location(surathLocationName3,surathLocationDesc3,"สุราษฎร์ธานี"));
        locationRepository.save(new Location(surathLocationName4,surathLocationDesc4,"สุราษฎร์ธานี"));
        locationRepository.save(new Location(surathLocationName5,surathLocationDesc5,"สุราษฎร์ธานี"));

        goodRepository.save(new Good(surathGoodName1, surathGoodDesc1, "สุราษฎร์ธานี"));
        goodRepository.save(new Good(surathGoodName2, surathGoodDesc2, "สุราษฎร์ธานี"));
        goodRepository.save(new Good(surathGoodName3, surathGoodDesc3, "สุราษฎร์ธานี"));
        goodRepository.save(new Good(surathGoodName4, surathGoodDesc4, "สุราษฎร์ธานี"));
        goodRepository.save(new Good(surathGoodName5, surathGoodDesc5, "สุราษฎร์ธานี"));

        locationRepository.save(new Location(chiangmaiLocationName1,chiangmaiLocationDesc1,"เชียงใหม่"));
        locationRepository.save(new Location(chiangmaiLocationName2,chiangmaiLocationDesc2,"เชียงใหม่"));
        locationRepository.save(new Location(chiangmaiLocationName3,chiangmaiLocationDesc3,"เชียงใหม่"));
        locationRepository.save(new Location(chiangmaiLocationName4,chiangmaiLocationDesc4,"เชียงใหม่"));
        locationRepository.save(new Location(chiangmaiLocationName5,chiangmaiLocationDesc5,"เชียงใหม่"));

        goodRepository.save(new Good(chiangmaiGoodName1, chiangmaiGoodDesc1, "เชียงใหม่"));
        goodRepository.save(new Good(chiangmaiGoodName2, chiangmaiGoodDesc2, "เชียงใหม่"));
        goodRepository.save(new Good(chiangmaiGoodName3, chiangmaiGoodDesc3, "เชียงใหม่"));
        goodRepository.save(new Good(chiangmaiGoodName4, chiangmaiGoodDesc4, "เชียงใหม่"));
        goodRepository.save(new Good(chiangmaiGoodName5, chiangmaiGoodDesc5, "เชียงใหม่"));
    }
}
