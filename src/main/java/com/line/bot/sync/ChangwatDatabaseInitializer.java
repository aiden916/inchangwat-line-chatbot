package com.line.bot.sync;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("changwatDatabaseInitializer")
public class ChangwatDatabaseInitializer implements InitializingBean {

    @Autowired
    private ChangwatDataSynchronizer changwatDataSynchronizer;

    @Override
    public void afterPropertiesSet() throws Exception {
        changwatDataSynchronizer.forceSync();
    }
}
