package com.line.bot.model;

import javax.persistence.*;

@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String locationName;

    private String description;

    private String changwat;

    public Location() {
    }

    public Location(String locationName, String description, String changwat) {
        this.locationName = locationName;
        this.description = description;
        this.changwat = changwat;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setChangwat(String changwat) {
        this.changwat = changwat;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getDescription() {
        return description;
    }

    public String getChangwat() {
        return changwat;
    }
}
