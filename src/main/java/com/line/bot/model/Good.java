package com.line.bot.model;

import javax.persistence.*;

@Entity
public class Good{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String goodName;
    private String description;
    private String changwat;

    public Good() {
    }

    public Good(String goodName, String description, String changwat) {
        this.goodName = goodName;
        this.description = description;
        this.changwat = changwat;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setChangwat(String changwat) {
        this.changwat = changwat;
    }

    public String getGoodName() {
        return goodName;
    }

    public String getDescription() {
        return description;
    }

    public String getChangwat() {
        return changwat;
    }
}
