package com.line.bot.service.good.message.Impl;

import com.line.bot.service.good.message.IGoodMessage;
import com.line.bot.model.Good;
import com.line.bot.repository.GoodRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodMessage implements IGoodMessage {

    @Autowired
    private GoodRepository goodRepository;

    private List<Good> goodList;

    @Value("${reply-message.ending-message}")
    private String endingMsg;

    Logger log = LoggerFactory.getLogger(GoodMessage.class);

    @Override
    public String constructMessage(String changwat) {
        log.info("Contructing Good Message . . .");
        goodList = goodRepository.findByChangwat(changwat);
        StringBuilder stringBuilder = new StringBuilder("รายชื่อของดียอดนิยมประจำจังหวัด" + changwat + "\n");
        for (int i = 0; i < goodList.size(); i++) {
            stringBuilder.append("G" + (i+1) + ". " + goodList.get(i).getGoodName() + "\n");
        }
        stringBuilder.append("\n" + endingMsg);
        return stringBuilder.toString();
    }

    @Override
    public String constructGoodDesc(int index){
        log.info("Constructing Good Description . . .");
        return "G" + index + ". " + goodList.get(index-1).getGoodName() + "\n\n" + goodList.get(index-1).getDescription();
    }
}
