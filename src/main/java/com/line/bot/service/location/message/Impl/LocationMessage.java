package com.line.bot.service.location.message.Impl;

import com.line.bot.service.location.message.ILocationMessage;
import com.line.bot.model.Location;
import com.line.bot.repository.LocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationMessage implements ILocationMessage {

    @Autowired
    private LocationRepository locationRepository;

    private List<Location> locationList;

    @Value("${reply-message.ending-message}")
    private String endingMsg;

    Logger log = LoggerFactory.getLogger(LocationMessage.class);

    @Override
    public String constructMessage(String changwat) {
        log.info("Contructing Location Message . . .");
        locationList = locationRepository.findByChangwat(changwat);
        StringBuilder stringBuilder = new StringBuilder("รายชื่อสถานที่ท่องเที่ยวยอดนิยมประจำจังหวัด" + changwat + "\n");
        for (int i = 0; i < locationList.size(); i++) {
            stringBuilder.append("L" + (i+1) + ". " + locationList.get(i).getLocationName() + "\n");
        }
        stringBuilder.append("\n" + endingMsg);
        return stringBuilder.toString();
    }

    @Override
    public String constructLocationDesc(int index){
        log.info("Constructing Location Description . . .");
        return "L" + index + ". " + locationList.get(index-1).getLocationName() + "\n\n" + locationList.get(index-1).getDescription();
    }
}
