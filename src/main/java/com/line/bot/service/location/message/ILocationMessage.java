package com.line.bot.service.location.message;

public interface ILocationMessage {

    String constructMessage(String changwat);

    String constructLocationDesc(int index);
}
