package com.line.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class LineBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(LineBotApplication.class, args);
    }
}
