package com.line.bot.controller;

import com.line.bot.service.good.message.IGoodMessage;
import com.line.bot.service.location.message.ILocationMessage;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.*;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

@Slf4j
@LineMessageHandler
public class LineBotController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private ILocationMessage locationMessage;

    @Autowired
    private IGoodMessage goodMessage;

    @Value("${reply-message.select-category}")
    private String categoryTextMsg;

    @Value("${reply-message.ending-message}")
    private String endingMsg;

    @Value("${reply-message.non-text}")
    private String replyNonTextMsg;

    @Value("${reply-message.invalid-message}")
    private String invalidMsg;

    @Value("${reply-message.follow-message}")
    private String followMsg;

    private String changwat;

    Logger log = LoggerFactory.getLogger(LineBotController.class);

    @EventMapping
    public void handleTextMessage(MessageEvent<TextMessageContent> event) {
        log.info("Receive Text Message . . .");
        TextMessageContent message = event.getMessage();
        String changwatFromMsg = getChangwatFromMsg(message.getText());

        if (!changwatFromMsg.isEmpty()) {
            changwat = changwatFromMsg;
            reply(event.getReplyToken(), new TextMessage("จังหวัด" + changwat + "\n" + categoryTextMsg + "\n\n" + endingMsg));

        } else {
            String index = event.getMessage().getText().toUpperCase();
            handleCategoryRequest(event.getReplyToken(), index);
        }
    }

    @EventMapping
    public void handleStickerMessage(MessageEvent<StickerMessageContent> event) {
        log.info("Receive Sticker Message . . .");
        handleNonTextMessage(event.getReplyToken());
    }

    @EventMapping
    public void handleLocationMessage(MessageEvent<LocationMessageContent> event) {
        log.info("Receive Location Message . . .");
        handleNonTextMessage(event.getReplyToken());
    }

    @EventMapping
    public void handleImageMessage(MessageEvent<ImageMessageContent> event) {
        log.info("Receive Image Message . . .");
        handleNonTextMessage(event.getReplyToken());
    }

    @EventMapping
    public void handleUnfollowMessage(UnfollowEvent event) {
        log.info(event.getSource().getUserId() + " Unfollowed Chat Bot . . .");
    }

    @EventMapping
    public void handleFollowEvent(FollowEvent event) {
        log.info(event.getSource().getUserId() + " Followed Chat Bot . . .");
        String replyToken = event.getReplyToken();
        reply(replyToken, new TextMessage(followMsg));
    }

    @EventMapping
    public void handleOtherEvent(Event event) {
        log.info("Received message(Ignored): {}", event + " . . .");
    }

    private void handleNonTextMessage(String replyToken) {
        reply(replyToken, new TextMessage(replyNonTextMsg));
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        try {
            log.info("Reply Message . . .");
            BotApiResponse response = lineMessagingClient.replyMessage(
                    new ReplyMessage(replyToken, messages)
            ).get();
            log.info("Reply Message Successfully . . .");
        } catch (InterruptedException | ExecutionException e) {
            log.info("Failed to Reply Message . . .");
            throw new RuntimeException(e);
        }
    }

    private String getChangwatFromMsg(String message) {
        String[] validChangwat = {"เชียงใหม่", "สุราษ"};
        for (String changwat : validChangwat) {
            if (message.contains(changwat)) {
                if (changwat.equals("สุราษ")) return "สุราษฎร์ธานี";
                return changwat;
            }
        }
        return "";
    }

    private void handleCategoryRequest(String replyToken, String index) {
        String inCase;
        if(changwat != null) {
            if (index.matches(".*[L][1-5]")) {
                inCase = "L";
            } else if (index.matches(".*[G][1-5]")) {
                inCase = "G";
            } else {
                inCase = index.replaceAll("[^1-5]", "");
            }
            handleCase(replyToken, index, inCase);
        }
        else reply(replyToken, new TextMessage(invalidMsg));
    }

    private void handleCase(String replyToken, String index, String inCase) {
        String replyMessage;
        switch (inCase) {
            case "1":
                replyMessage = locationMessage.constructMessage(changwat);
                reply(replyToken, new TextMessage(replyMessage));
                break;
            case "2":
                replyMessage = goodMessage.constructMessage(changwat);
                reply(replyToken, new TextMessage(replyMessage));
                break;
            case "L":
                index = index.replaceAll("[^1-5]", "");
                replyMessage = locationMessage.constructLocationDesc(Integer.parseInt(index.substring(0,1)));
                reply(replyToken, new TextMessage(replyMessage));
                break;
            case "G":
                index = index.replaceAll("[^1-5]", "");
                replyMessage = goodMessage.constructGoodDesc(Integer.parseInt(index.substring(0,1)));
                reply(replyToken, new TextMessage(replyMessage));
                break;
            default:
                reply(replyToken, new TextMessage(invalidMsg));
                break;
        }
    }
}
