package com.line.bot.repository;

import com.line.bot.model.Location;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {
    @Query("SELECT m FROM Location m where m.changwat LIKE %:keyword%")
    List<Location> findByChangwat(@Param("keyword") String keyword);
}
