package com.line.bot.repository;

import com.line.bot.model.Good;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodRepository extends CrudRepository<Good, Integer> {
    @Query("SELECT m FROM Good m where m.changwat LIKE %:keyword%")
    List<Good> findByChangwat(@Param("keyword") String keyword);
}
